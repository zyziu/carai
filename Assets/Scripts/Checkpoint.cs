﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {
    [SerializeField] Material defaultMaterial;
    [SerializeField] Material passedMaterial;

    new Renderer renderer;
    bool passed;

    public bool IsPassed {
        get { return passed; }
    }

    void Awake() {
        renderer = GetComponent<Renderer>();
        passed = false;
    }

    public void SetBool(bool passed) {
        this.passed = passed;
        renderer.material = this.passed ? passedMaterial : defaultMaterial;
    }
}