﻿using UnityEngine;
using System.Collections;

public class Hit : MonoBehaviour {
    public int checkpoints;
    public Material Passed;
    public bool crash;

    Agent agent;

    void Awake() {
        agent = gameObject.GetComponent<Agent>();
        crash = false;
        checkpoints = 0;
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Checkpoint") {
            Checkpoint checkpoint = other.gameObject.GetComponent<Checkpoint>();
            if (checkpoint.IsPassed == false) {
                checkpoint.SetBool(true);
                checkpoints++;
                agent.dist += 1.0f;
            }
        }
        else {
            //Hit a wall considered fail
            //checkpoints = 0;
            crash = true;
        }
    }
}