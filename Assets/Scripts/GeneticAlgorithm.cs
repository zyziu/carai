﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GeneticAlgorithm {
    public int populationSize = 16;
    public int chromosomeLength = 66;

    int generation;
    int currentGenome;

    const float MUTATION_RATE = 0.15f;
    const float MAX_PERBETUATION = 0.3f;

    public List<Genome> population = new List<Genome>();
    double totalFitnessScore;

    public bool IsLastGenome { get { return currentGenome == populationSize - 1; } }

    public GeneticAlgorithm() {
        currentGenome = -1;
        generation = 1;
    }

    public Genome GetNextGenome() {
        currentGenome++;
        if (currentGenome >= population.Count)
            return null;

        return population[this.currentGenome];
    }

    public int GetCurrentGeneration() {
        return generation;
    }

    public int GetCurrentGenomeIndex() {
        return currentGenome;
    }

    public void Crossover(List<float> mom, List<float> dad, List<float> baby1, List<float> baby2) {
        int genomeLength = mom.Count;

        int crossoverPoint = (int) Random.Range(0, genomeLength - 1);

        //Go from start to crossover point, copying the weights from mom
        for (int i = 0; i < crossoverPoint; i++) {
            baby1.Add(mom[i]);
            baby2.Add(dad[i]);
        }

        for (int i = crossoverPoint; i < genomeLength; i++) {
            baby1.Add(dad[i]);
            baby2.Add(mom[i]);
        }
    }

    public void GenerateNewPopulation() {
        generation = 1;
        currentGenome = -1;
        ClearPopulation();
        if (population.Count < populationSize) {
            for (int i = population.Count; i < populationSize; i++) {
                population.Add(new Genome(chromosomeLength));
            }
        }
    }

    public void BreedPopulation() {
        List<Genome> children = new List<Genome>();

        int numberOfNewBabies = 0;
        while (numberOfNewBabies < populationSize) {
            // select 2 parents
            Genome mom = RouletteWheelSelection();
            Genome dad = RouletteWheelSelection();
            Genome baby1 = new Genome();
            Genome baby2 = new Genome();
            Crossover(mom.weights, dad.weights, baby1.weights, baby2.weights);
            Mutate(baby1.weights);
            Mutate(baby2.weights);
            children.Add(baby1);
            children.Add(baby2);

            numberOfNewBabies += 2;
        }

        ClearPopulation();
        population = children;

        currentGenome = 0;
        generation++;
    }

    public Genome RouletteWheelSelection() {
        totalFitnessScore = population.Sum(x => x.fitness);
        double slice = UnityEngine.Random.value*totalFitnessScore;
        double total = 0;
        int selectedGenome = 0;

        for (int i = 0; i < populationSize; i++) {
            total += population[i].fitness;

            if (total > slice) {
                selectedGenome = i;
                break;
            }
        }
        return population[selectedGenome];
    }

    public void ClearPopulation() {
        for (int i = 0; i < population.Count; i++) {
            if (population[i] != null) {
                population[i] = null;
            }
        }
        population.Clear();
    }

    public void Mutate(List<float> genome) {
        for (int i = 0; i < genome.Count; i++) {
            if (RandomWeight() < MUTATION_RATE) {
                genome[i] += (RandomWeight()*MAX_PERBETUATION);
            }
        }
    }

    public void SetGenomeFitness(float fitness) {
        if (currentGenome < population.Count) {
            population[currentGenome].fitness = fitness;
        }
    }

    public float RandomWeight() {
        return Random.Range(-1f, 1f);
    }
}