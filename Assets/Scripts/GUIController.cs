﻿using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour {
    [SerializeField] Text currentFitness;
    [SerializeField] Text bestFitness;
    [SerializeField] Text generation;
    [SerializeField] Text genomeCount;

    Entity entity;

    void Awake() {
        entity = FindObjectOfType<Entity>();
    }

    void Update() {
        currentFitness.text = entity.GetCurrentFitness();
        bestFitness.text = entity.GetBestFitness();
        generation.text = entity.GetGenerationNumber();
        genomeCount.text = entity.GetGenomeCount();

    }
}
