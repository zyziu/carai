﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Entity : MonoBehaviour {
    float currentAgentFitness;
    float bestFitness;
    float currentTimer;

    public NNet neuralNet;

    Agent testAgent;
    GeneticAlgorithm geneticAlgorithm;
    List<Checkpoint> checkpointList;
    Hit hit;

    Vector3 defaultpos;
    Quaternion defaultrot;

    void Awake() {
        checkpointList = GameObject.FindObjectsOfType<Checkpoint>().ToList();
        geneticAlgorithm = new GeneticAlgorithm();
        geneticAlgorithm.GenerateNewPopulation();
        currentAgentFitness = 0.0f;
        bestFitness = 0.0f;

        neuralNet = new NNet();
        neuralNet.CreateNet(1, 5, 8, 2);
        Genome genome = geneticAlgorithm.GetNextGenome();
        neuralNet.FromGenome(genome, 5, 8, 2);

        testAgent = gameObject.GetComponent<Agent>();
        testAgent.Attach(neuralNet);

        hit = gameObject.GetComponent<Hit>();
        defaultpos = transform.position;
        defaultrot = transform.rotation;
    }

    void Update() {
        if (testAgent.hasFailed) {
            if (geneticAlgorithm.IsLastGenome) {
                EvolveGenomes();
            }
            NextTestSubject();
        }
        currentAgentFitness = testAgent.dist;
        if (currentAgentFitness > bestFitness) {
            bestFitness = currentAgentFitness;
        }
    }

    public void NextTestSubject() {
        geneticAlgorithm.SetGenomeFitness(currentAgentFitness);
        currentAgentFitness = 0.0f;
        Genome genome = geneticAlgorithm.GetNextGenome();

        neuralNet.FromGenome(genome, 5, 8, 2);

        transform.position = defaultpos;
        transform.rotation = defaultrot;
        testAgent.Attach(neuralNet);
        testAgent.ClearFailure();

        checkpointList.ForEach(x => x.SetBool(false));
    }

    public void EvolveGenomes() {
        geneticAlgorithm.BreedPopulation();
    }

    public string GetCurrentFitness() {
        return currentAgentFitness.ToString();
    }

    public string GetBestFitness() {
        return bestFitness.ToString();
    }

    public string GetGenerationNumber() {
        return geneticAlgorithm.GetCurrentGenomeIndex() + " of " + geneticAlgorithm.populationSize;
    }

    public string GetGenomeCount() {
        return geneticAlgorithm.GetCurrentGeneration().ToString();
    }
}